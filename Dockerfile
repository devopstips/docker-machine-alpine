FROM alpine:latest
RUN apk update && \
    apk add openssl && \
    wget https://github.com/docker/machine/releases/download/v0.10.0/docker-machine-`uname -s`-`uname -m` \
    -O /usr/local/bin/docker-machine && \
    chmod +x /usr/local/bin/docker-machine

